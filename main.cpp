#include "Character.h"
#include "Enemy.h"
#include "Prop.h"
#include "Utils.h"
#include <map>
#include <raylib.h>
#include <raymath.h>
#include <string>
#include <iostream>

const Dimensions<int> window{384, 384};

int main() {
    std::cout << "Run path: " << GetWorkingDirectory() << std::endl;

    InitWindow(window.width, window.height, "Classy Clash Game");

    std::map<std::string, Texture2D> textures{};

    for (auto const &[key, path]: assetsPaths) { textures.insert({key, LoadTexture(path)}); }

    Vector2 mapPosition{};

    Character knight{window.width, window.height, textures.at("knightIdle"), textures.at("knightRun"),
                     textures.at("sword")};
    Prop props[2]{Prop{{600.f, 300.f}, textures.at("rock")}, Prop{{400.f, 500.f}, textures.at("log")}};

    Enemy goblin1{Vector2{1600.f, 300.f}, 2.f, textures.at("goblinIdle"), textures.at("goblinRun")};
    Enemy goblin2{Vector2{500.f, 1900.f}, 3.f, textures.at("goblinIdle"), textures.at("goblinRun")};

    Enemy *enemies[]{&goblin1, &goblin2};

    for (Enemy *enemy: enemies) { enemy->setTarget(&knight); }

    SetTargetFPS(60);
    while (!WindowShouldClose()) {
        BeginDrawing();
        ClearBackground(WHITE);

        mapPosition = Vector2Scale(knight.getWorldPos(), -1.f);
        DrawTextureEx(textures.at("worldMap"), mapPosition, 0.f, scale, WHITE);

        for (Prop prop: props) { prop.Render(knight.getWorldPos()); }

        knight.tick(GetFrameTime());
        if (knight.getWorldPos().x < 0.f || knight.getWorldPos().y < 0.f ||
            knight.getWorldPos().x + static_cast<float>(window.width) >
                    static_cast<float>(textures.at("worldMap").width) * scale ||
            knight.getWorldPos().y + static_cast<float>(window.height) >
                    static_cast<float>(textures.at("worldMap").height) * scale) {
            knight.undoMovement();
        }

        for (Prop prop: props) {
            if (CheckCollisionRecs(knight.getCollisionRect(), prop.getCollisionRect(knight.getWorldPos())))
                knight.undoMovement();
        }

        if (!knight.getAlive()) {
            DrawText("Game Over!", 55.f, 45.f, 40, RED);

            EndDrawing();
            continue;
        } else {
            std::string knightsHealth = "Health: ";
            knightsHealth.append(std::to_string(knight.getHealth()), 0, 5);

            DrawText(knightsHealth.c_str(), 55.f, 45.f, 40, RED);
        }

        for (Enemy *goblin: enemies) {
            goblin->tick(GetFrameTime());

            if (IsMouseButtonPressed(MOUSE_BUTTON_LEFT)) {
                if (CheckCollisionRecs(goblin->getCollisionRect(), knight.getWeaponCollisionRect())) {
                    goblin->setAlive(false);
                }
            }
        }

        EndDrawing();
    }

    for (auto const &[key, texture]: textures) { UnloadTexture(texture); }

    return 0;
}
