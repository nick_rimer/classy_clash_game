#include "BaseCharacter.h"
#include <raymath.h>

BaseCharacter::BaseCharacter() = default;

Rectangle BaseCharacter::getCollisionRect() {
    return Rectangle{getScreenPos().x, getScreenPos().y, frameDimensions.width * scale, frameDimensions.height * scale};
}

void BaseCharacter::undoMovement() { worldPos = worldPosLastFrame; }

void BaseCharacter::tick(float deltaTime) {
    worldPosLastFrame = worldPos;

    // update animation frame
    runningTime += deltaTime;
    if (runningTime >= updateTime) {
        frame += 1;
        runningTime = 0.f;

        if (frame > maxFrames) frame = 0;
    }

    // update movement
    if (Vector2Length(velocity) != 0) {
        worldPos = Vector2Add(worldPos, Vector2Scale(Vector2Normalize(velocity), getSpeed()));

        velocity.x < 0 ? facing = -1.f : facing = 1.f;

        textureCurrent = textureRun;
    } else {
        textureCurrent = textureIdle;
    }
    velocity = Vector2{};

    // draw
    const Rectangle knightRectangleSource{static_cast<float>(frame) * frameDimensions.width, 0.f,
                                          facing * frameDimensions.width, frameDimensions.height};
    const Rectangle knightRectangleDest{getScreenPos().x, getScreenPos().y, scale * frameDimensions.width,
                                        scale * frameDimensions.height};
    DrawTexturePro(textureCurrent, knightRectangleSource, knightRectangleDest, Vector2{}, 0.f, WHITE);
}
