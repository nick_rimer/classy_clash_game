#include "Enemy.h"
#include <raymath.h>

Enemy::Enemy(Vector2 pos, float enemySpeed, Texture2D idleTexture, Texture2D runTexture) {
    worldPos = pos;
    speed = enemySpeed;
    textureCurrent = idleTexture;
    textureIdle = idleTexture;
    textureRun = runTexture;
    frameDimensions = {static_cast<float>(textureCurrent.width) / static_cast<float>(maxFrames),
                       static_cast<float>(textureCurrent.height)};
}

void Enemy::tick(float deltaTime) {
    if (!getAlive()) return;

    velocity = Vector2Subtract(target->getScreenPos(), getScreenPos());
    if (Vector2Length(velocity) < radius) velocity = Vector2{};

    BaseCharacter::tick(deltaTime);

    if (CheckCollisionRecs(getCollisionRect(), target->getCollisionRect())) {
        target->takeDamage(damagePerSec * deltaTime);
    }
}

Vector2 Enemy::getScreenPos() { return Vector2Subtract(worldPos, target->getWorldPos()); }
