#include <map>
#include <string>

std::map<std::string, const char *> assetsPaths{{"worldMap", "../assets/worldMap.png"},
                                                {"knightIdle", "../assets/character/knight_idle_spritesheet.png"},
                                                {"knightRun", "../assets/character/knight_run_spritesheet.png"},
                                                {"goblinIdle", "../assets/enemy/goblin/goblin_idle_spritesheet.png"},
                                                {"goblinRun", "../assets/enemy/goblin/goblin_run_spritesheet.png"},
                                                {"rock", "../assets/obstacles/rock.png"},
                                                {"log", "../assets/obstacles/log.png"},
                                                {"sword", "../assets/weapon/sword/weapon_sword.png"}};
