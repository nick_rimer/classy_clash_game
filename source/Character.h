#ifndef CLASSY_CLASH_CHARACTER_H
#define CLASSY_CLASH_CHARACTER_H

#include "BaseCharacter.h"
#include <raylib.h>

class Character : public BaseCharacter {
public:
    Character(int windowWidth, int windowHeight, Texture2D idleTexture, Texture2D runTexture, Texture2D weaponTexture);
    void tick(float deltaTime) override;
    Vector2 getScreenPos() override;
    Rectangle getWeaponCollisionRect() { return weaponCollisionRect; }
    float getHealth() const { return health; }
    void takeDamage(float damage);

private:
    int windowWidth{};
    int windowHeight{};
    Texture2D weapon{};
    Rectangle weaponCollisionRect{};
    float rotation{};
    float health{100.f};
};

#endif
