#include "Prop.h"
#include <raymath.h>

Prop::Prop(Vector2 pos, Texture2D tex) : position(pos), texture(tex) {}

void Prop::Render(Vector2 knightPosition) {
    DrawTextureEx(texture, Vector2Subtract(position, knightPosition), 0.f, scale, WHITE);
}

Rectangle Prop::getCollisionRect(Vector2 knightPosition) {
    Vector2 screenPosition = Vector2Subtract(position, knightPosition);

    return Rectangle{screenPosition.x, screenPosition.y, static_cast<float>(texture.width) * scale,
                     static_cast<float>(texture.height) * scale};
}
