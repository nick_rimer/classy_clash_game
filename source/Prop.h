#ifndef CLASSY_CLASH_PROP_H
#define CLASSY_CLASH_PROP_H

#include "Utils.h"
#include <raylib.h>

class Prop {
public:
    Prop(Vector2 pos, Texture2D tex);
    void Render(Vector2 knightPosition);
    Rectangle getCollisionRect(Vector2 knightPosition);

private:
    Vector2 position{};
    Texture2D texture{};
};

#endif
