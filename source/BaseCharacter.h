#ifndef CLASSY_CLASH_BASECHARACTER_H
#define CLASSY_CLASH_BASECHARACTER_H

#include "Utils.h"
#include <raylib.h>

class BaseCharacter {
public:
    BaseCharacter();
    Vector2 getWorldPos() const { return worldPos; }
    Rectangle getCollisionRect();
    void undoMovement();
    virtual void tick(float deltaTime);
    virtual Vector2 getScreenPos() = 0;
    bool getAlive() const { return alive; }
    void setAlive(bool isAlive) { alive = isAlive; }
    float getSpeed() const { return speed; }
    void setSpeed(float characterSpeed) { speed = characterSpeed; }

protected:
    Texture2D textureIdle{};
    Texture2D textureRun{};
    Texture2D textureCurrent{};
    Vector2 worldPos{100.f, 100.f};
    Vector2 worldPosLastFrame{};
    float facing{1.f};
    float runningTime{};
    int frame{};
    const int maxFrames{6};
    const float updateTime{1.f / 12.f};
    float speed{4.f};
    Dimensions<float> frameDimensions{};
    Vector2 velocity{};

private:
    bool alive{true};
};

#endif
