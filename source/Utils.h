#ifndef CLASSY_CLASH_UTILS_H
#define CLASSY_CLASH_UTILS_H

#include <map>
#include <string>

template<class T>
struct Dimensions {
    T width;
    T height;
};

extern std::map<std::string, const char *> assetsPaths;

const float scale{4.f};

#endif
