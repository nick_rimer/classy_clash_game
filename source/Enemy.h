#ifndef CLASSY_CLASH_ENEMY_H
#define CLASSY_CLASH_ENEMY_H

#include "BaseCharacter.h"
#include "Character.h"
#include <raylib.h>

class Enemy : public BaseCharacter {
public:
    Enemy(Vector2 pos, float enemySpeed, Texture2D idleTexture, Texture2D runTexture);
    void tick(float deltaTime) override;
    void setTarget(Character *pCharacter) { target = pCharacter; };
    Vector2 getScreenPos() override;

private:
    Character *target{};
    float damagePerSec{10.f};
    float radius{25.f};
};

#endif
