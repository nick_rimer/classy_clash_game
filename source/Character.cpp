#include "Character.h"
#include <raymath.h>

Character::Character(int winWidth, int winHeight, Texture2D idleTexture, Texture2D runTexture, Texture2D weaponTexture)
    : windowWidth(winWidth), windowHeight(winHeight), weapon(weaponTexture) {
    textureCurrent = idleTexture;
    textureIdle = idleTexture;
    textureRun = runTexture;

    frameDimensions = {static_cast<float>(textureCurrent.width) / static_cast<float>(maxFrames),
                       static_cast<float>(textureCurrent.height)};
};

Vector2 Character::getScreenPos() {
    return Vector2{0.5f * (static_cast<float>(windowWidth) - scale * frameDimensions.width),
                   0.5f * (static_cast<float>(windowHeight) - scale * frameDimensions.height)};
}

void Character::tick(float deltaTime) {
    if (!getAlive()) return;

    if (IsKeyDown(KEY_A)) velocity.x -= 1.f;
    if (IsKeyDown(KEY_D)) velocity.x += 1.f;
    if (IsKeyDown(KEY_W)) velocity.y -= 1.f;
    if (IsKeyDown(KEY_S)) velocity.y += 1.f;

    BaseCharacter::tick(deltaTime);

    // draw weapon
    Vector2 weaponOrigin{};
    Vector2 weaponOffset{};
    if (facing > 0.f) {
        weaponOrigin = {0.f, static_cast<float>(weapon.height) * scale};
        weaponOffset = {36.f, 55.f};
        weaponCollisionRect = {getScreenPos().x + weaponOffset.x,
                               getScreenPos().y + weaponOffset.y - static_cast<float>(weapon.height) * scale,
                               static_cast<float>(weapon.width) * scale, static_cast<float>(weapon.height) * scale};
        rotation = IsMouseButtonDown(MOUSE_BUTTON_LEFT) ? 35.f : 0.f;
    } else {
        weaponOrigin = {static_cast<float>(weapon.width) * scale, static_cast<float>(weapon.height) * scale};
        weaponOffset = {28.f, 55.f};
        weaponCollisionRect = {getScreenPos().x + weaponOffset.x - static_cast<float>(weapon.width) * scale,
                               getScreenPos().y + weaponOffset.y - static_cast<float>(weapon.height) * scale,
                               static_cast<float>(weapon.width) * scale, static_cast<float>(weapon.height) * scale};
        rotation = IsMouseButtonDown(MOUSE_BUTTON_LEFT) ? -35.f : 0.f;
    }

    const Rectangle weaponRectangleSource{0.f, 0.f, static_cast<float>(weapon.width) * facing,
                                          static_cast<float>(weapon.height)};
    const Rectangle weaponRectangleDestination{getScreenPos().x + weaponOffset.x, getScreenPos().y + weaponOffset.y,
                                               static_cast<float>(weapon.width) * scale,
                                               static_cast<float>(weapon.height) * scale};
    DrawTexturePro(weapon, weaponRectangleSource, weaponRectangleDestination, weaponOrigin, rotation, WHITE);

    /*DrawRectangleLines(static_cast<int>(weaponCollisionRect.x), static_cast<int>(weaponCollisionRect.y),
                       static_cast<int>(weaponCollisionRect.width), static_cast<int>(weaponCollisionRect.height), RED);*/
}

void Character::takeDamage(float damage) {
    health -= damage;
    if (health < 0.f) setAlive(false);
}
